package com.mared;

import java.io.IOException;
import java.util.StringTokenizer;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;

import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
public class SizeandWordCount {
	 
	public static class TokenizerMapper
      extends Mapper<Object, Text, IntWritable,Text >{

   private  IntWritable size ;
   private Text word = new Text();
   private String temp_str; 
   public void map(Object key, Text value, Context context
                   ) throws IOException, InterruptedException {
     StringTokenizer itr = new StringTokenizer(value.toString());
     while (itr.hasMoreTokens()) 
     {
       temp_str=itr.nextToken();
       word.set(temp_str);
       size = new IntWritable(temp_str.length());
       context.write(size,word);
     }
   }
 }

 public static class IntSumReducer
      extends Reducer<IntWritable,Text,IntWritable,Text> {
   private IntWritable result = new IntWritable();
   
   public void reduce(IntWritable key, Iterable<Text> values,
                      Context context
                      ) throws IOException, InterruptedException {
     Text final_string =new Text();
     String sum="";
     int cnt=0;
     for (Text val : values) {
       sum=sum.concat(val.toString() + (val.toString().length()>0 ? "," : "") );
    	 //sum=sum.concat(val.toString() + ",");
       cnt++;
     }
     sum=sum.substring(0,sum.length()-1);
     result.set(cnt);      
     final_string.set("(As the word of size "+key.get()+" are "+sum+")");
    // System.out.println(key+","+sum);     
     context.write(result, final_string);
     
   }
 }

 public static void main(String[] args) throws Exception {
   Configuration conf = new Configuration();
   Job job = Job.getInstance(conf, "word count");
   job.setJarByClass(WordCount.class);
   job.setMapperClass(TokenizerMapper.class);
   //job.setCombinerClass(IntSumReducer.class);
   job.setReducerClass(IntSumReducer.class);   
   job.setOutputKeyClass(IntWritable.class);
   job.setOutputValueClass(Text.class);
   FileInputFormat.addInputPath(job, new Path(args[0]));
  // FileInputFormat.addInputPath(job, new Path("/home/hadoop/hadoop/pandee_mapred/sizecount_in"));
   FileOutputFormat.setOutputPath(job, new Path(args[1]));
 //  FileOutputFormat.setOutputPath(job, new Path("/home/hadoop/hadoop/pandee_mapred/sizecount_out11"));
   System.exit(job.waitForCompletion(true) ? 0 : 1);
 }
}
